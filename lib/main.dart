
//Name: Jesus Mercado
//Course: CS 481 (Mobile Programming)
//Semester: Fall 2020
//Assignment: Homework 3  (Stateful Widgets)

import 'dart:ui';
import 'package:flutter/material.dart';
//import 'globals.dart' as globals;


//runs the app
void main() => runApp(MyApp());

//Here we declare the main widget for the app
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //Lets adjust the theme of the app
      theme: ThemeData(
        brightness: Brightness.dark,  //gives us dark background and white text
        primaryColor: Colors.green,   //Makes the appBar green
      ),
      //Next let us declare the home screen of the app
      home: Scaffold(
        //We will have an appBar on our home screen
        appBar: AppBar(
          //this appBar will have a title which is centered and says My Pets
          title: Center(
            child: Text('My Pets'),
          ),
        ),
        //Now let us define the body of the home screen (everything under appBar
        //We declare a Single child scroll view since all our widgets will be
        //declared within a Container
        body:  SingleChildScrollView(
          //We declare a child for this scroll view which will be the
          //container that holds all our widgets
          child: Container(
              //Now in order to add a background image to the home screen, we
              //must make the desired background image, a decoration image for
              //the container.
              //We declare a decoration for the box
              decoration: BoxDecoration(
                //We declare an image for this decoration
                image: DecorationImage(
                  //Now we use an asset image as the decoration
                  //image for our main container and we ensure it covers
                  //the entire container.
                  image: AssetImage('assets/Jungle.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
              //Within our main container we will have a row as the child
              //(only so we are able to center our column in the app home
              // screen, using just Column as the child for the main container
              //would result in crossAxis Alignment failing to center the
              //column in the app home screen)
              child: Row(
                //We ensure the main column (column with all information
                //widgets on the home screen) is centered on phone screen
                mainAxisAlignment: MainAxisAlignment.center,
                //we will then declare a child for this row which will be
                //the main column
                children: [
                  //Main Info Column for app
                  Column(
                    //We then declare children for the main column which
                    //will be the all the info widgets of the app
                    children: [
                      //Note: We have defined _buildMainContainer(),
                      //_buildInfoContainer(), and
                      //_buildBottomContainer() below under the app
                      //declaration.
                      //Now we add the Main container to our app at
                      //the top of the body, directly under appBar.
                      //This will have the image of all the birds
                      //together, a title, and a brief description
                      // of parakeets
                      _buildMainContainer(),
                      //Under the main container, we will have a text
                      //which will indicate to continue scrolling to
                      //see the info and pic for each individual bird.
                      Text('Lets Meet Them...',
                        //We want to change the style of this text
                        style: TextStyle(
                          //se we declare a font size of 20
                          fontSize: 30,
                          //we change it for the theme's default
                          //white to black.
                          color: Colors.black,
                          //We change the font to Times New Roman
                          fontFamily: 'Times New Roman',
                        ),
                      ),
                      Container(
                        height: 40,
                       width: 300,
                       // color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Click on the box to go to the birds individual page....',
                                //textAlign: TextAlign.end,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 10,
                                )
                            ),
                          ],
                        )
                      ),
                      //Now, under this Text we will have the
                      //container which will hold all the information
                      //containers for each individual bird (holds
                      //4 containers, 1 for each bird).
                      _buildInfoContainer(),
                      //Under the Info Container we will have a
                      //Container which will act as the artist
                      //signature for the app, it will have CSUSM,
                      //the current semester, the class, and my name.
                      _buildBottomContainer(),
                    ],
                  ),
                ],
              ),
            ),
        ),
      ),
        );
  }
}
//We have now closed the declaration of the app


//However, lets look into the function definitions we used above. Down below
//will be the definition for each function.
//Lets begin with declaring _buildBirdContainer, which will be the function that
//generates a block container which will hold an image in one column, and the
//personal info of each bird in an adjacent column.
//(Used in _buildInfoContainer() to facilitate the building of each individual
//bird's container.
Widget _buildBirdContainer(String Name, String Picture, String Sex, String Owner, String desc, int v, int f, int l, BuildContext context, bool firstView, bool like, bool fed) => Container(
  //NOTE: WE HAVE 12 PARAMETERS (NAME, PICTURE, SEX, OWNER, DESC, V, F, etc.),
  //we must use in the function call for this function.
  //Each bird's container will be scaled according to screen size
  height: MediaQuery.of(context).size.height/1.13,
  width: MediaQuery.of(context).size.width,

  //Now, for this container we want it to act as a big column
  child:  Column(
    children: [
      //The 1st container hold the image of the individual bird and
      //ensures that this images takes up a certain portion of any given screen
      // (by making the container, the image
      //is in, a certain size) and we want our image to cover this
      //Container. so that the top portion of our screen has the image of the bird
      Container(
        child:  Image.asset(
          Picture,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height/3,
          fit: BoxFit.cover,
        ),
      ),

      //The 2nd container will be under the 1st container and we will have a column of Row containers
      //which will store two separate texts in a single row with desired
      // spacing
      //NOTE: The semantics/logic for all row containers will be
      //the same so I will only go into detail on the 1st row container
      //and just simply know the rest do the exact same thing but with
      //different strings (i.e Name:, Owner, Sex, etc).
      Container(
        //Referencing the 2nd container in the Bird's Info container,
        //adjacent to the 1st container. We want this container to then
        //fill the rest of the Bird's Info container, so we declare that
        //we want this container to take up 200 x 200 px which takes up
        //the remaining unused width of the Bird's Info Container and it
        //fills the height as well.
       // color: Colors.teal,
        height: MediaQuery.of(context).size.height/2.5,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(top: 5),
        //To have this container act as a column container as
        //desired, we have the child of the container be a column
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //Within this column container we will then create a
          //series of row containers which will each hold two
          //separate column containers in it. Essentially, creating
          //a row with two columns in it
          children: [
            //Lets declare 1st row container for name of the bird
            Container(
              //We want the height to adjust based on the text,
              //so we will not preset the height, but we do want
              //our width to take up the entire width of our
              //parent/column container
              child: Row(
                //Now that we have our row container we want to
                //create the 2 columns/column containers within
                //this row. One for the string "Name:" and the
                //other for the variable Name which is passed
                //into this function as a string and holds the
                //string that is the actual name of the bird.
                children: [
                  //We do not want to split the row in half,
                  //instead we want to have the strings
                  //identifying the info take up only a big
                  //enough portion of the row to fit the
                  //biggest ID string (i.e "Fun Fact:")
                  Container(
                    //In this case "Fun Fact:" takes aprox.
                    //60 px, again we do not define height
                    //because we want it to adjust to text
                    //height, but we only want the width to
                    //take up 65 px of the 200 px of the
                    //row
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text('Name:',
                      style: TextStyle(
                        fontSize: 14,
                         color: Colors.black,
                      ),
                      //In order to ensure that the text is
                      //aligned with the right side of this
                      //container, we align the text to the
                      //end aka right side of container
                      textAlign: TextAlign.center,
                    ),
                  ),
                  //Now our second column container within the
                  //row will take up the remaining 135 px in
                  //the row and will hold the contain the
                  //string passed into the function for name
                  Container(
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text(Name,
                      style: TextStyle(
                        fontSize: 14,
                          color: Colors.black,
                      ),
                      //To ensure that the text in this
                      //column of the row is centered
                      //and not directly next to the text
                      //in the above container, we align
                      //the text in this container to the
                      //center of the container
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            //Lets declare 2nd row container for sex of the bird
            Container(
              //width: MediaQuery.of(context).size.width/2.0,
              child: Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text('Sex:',
                      style: TextStyle(
                        fontSize: 14,
                          color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text(Sex,
                      style: TextStyle(
                        fontSize: 14,
                         color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            //Lets declare 3rd row container for Owner of the bird
            Container(
             // width: 200,
              child: Row(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text('Owner:',
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text(Owner,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            //Lets declare 4th row container for Fun Fact of bird
            Container(
              //width: MediaQuery.of(context).size.width/2.0,
                    width: MediaQuery.of(context).size.width/2.0,
                    child: Text(('The Great $Name'),
                      style: TextStyle(
                        fontSize: 18,
                          color: Colors.black,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    //Helps ensure that the smaller text
                    //size difference does not affect the
                    //formatting of the 2nd column
                    //container (smaller text starts at
                    //the same height as the larger text
                    //in the 1st column container for this
                    //row
                   // color: Colors.red,
                    padding: const EdgeInsets.only(
                        top: 3.5,
                        left: 2,
                        right: 2),
                    width: MediaQuery.of(context).size.width/2,
                    //Note: Here we do limit the height
                    //because we want the bottom 80 px of
                    //the Main column, that holds all of
                    //these row columns, to be left
                    //available for our view/feed
                    //Icons and data
                    //height: 80,
                    child: Text(desc,
                      //We want our text to form a block
                      //within the given column container
                      //space. So, we justify the
                      //alignment to get the text to fill
                      //the box evenly and since we deal
                      //with a block of text we lower the
                      //font size
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.black,
                      ),
                    ),
                  ),
            //Now under the 4 row containers for the bird's info,
            //we will have a container which takes up the bottom
            //200 x 72 px of the parent column container, which
            //holds all these row containers. Inside of this
            //container we will have two Icons with data
            //underneath(in a column).
            Container(
              padding: const EdgeInsets.only(left: 5, right: 5),
          //   color: Colors.yellow,
              width: 300,
              height: 84,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //build the essentially stateless icon butons to show
                //what the likes,feeds, and views are for a single bird
                //either on their page or on the home screen
                children: [
                  viewContainer(
                    views: v,
                    isviewed: firstView,
                  ),

                  feedsContainer(
                    feeds: f,
                    isfed: fed,
                  ),

                  likesContainer(
                    likes: l,
                    isliked: like,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ],
  ),
);
//Now we have declared the function for building each individual bird's
//info Page easily


//Lets now define the _buildMainContainer() which takes no parameters since
//everything in this container is explicitly defined.
Widget _buildMainContainer() => Container(
  //We will make this a column container, so that it can hold the Main block
  //of the app in a column since we want our widgets underneath one another
  child: Column(
    //To add each widget we will put them in containers within this column
    children: [
      //The 1st container will hold the image of all 4 birds together
      Container(
        //To ensure that the image will always take up the entire
        //width of any screen and only a certain height of the screen
        //we must envoke a layout builder which will return a
        //container with the image.
        child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Container(
                //Here we ensure the above mentioned constraints
                height: MediaQuery.of(context).size.height / 2.5,
                width: MediaQuery.of(context).size.width / 1,
                //Here we add the image to the container and ensure
                //it covers the entire container/desired area
                child: Image.asset('assets/Group.jpg',
                  fit: BoxFit.cover,
                ),
                //Ensure to close the layout builder with a ";" after
                //the parenthesis ending the definition of the
                //returned container
              );
            }
        ),
      ),
      //The 2nd container will hold the title for this main block of info
      Container(
        //ensure that the title is not directly beneath the image with
        //no spacing, we add padding to the top
        padding: const EdgeInsets.only(top: 5),
        child: Text('THE GREAT PARAKEETS OF MY HOME',
          //We want this text to be of Times New Roman font, size 20,
          //bold, and of color blueGrey.
          style: TextStyle(
            fontFamily: 'Times New Roman',
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.blueGrey,
          ),
        ),
      ),
      //The 3rd container will hold the paragraph with info for this Main
      //Container
      Container(
        //To ensure the block of text does not run from end to end
        //on the screen and to ensure that it is not directly under the
        //title with no space, we once again add padding
        padding: const EdgeInsets.only(
            left: 10,
            right: 10,
            top:10
        ),
        //We want this text box to only take up 200 x 380 px under
        //the title and image.
        height: 200,
        width: 380,
        child: Text(
          'Parakeets are part of many small to medium-sized species of parrots, '
              'generally they have a long tail and feathers. '
              'Parakeet is derived from the French word perroquet. However, '
              'in French parakeet is perruche. Parakeets often breed '
              'in groups, because they are encouraged by the presence of other parakeets. '
              'There could be conflicts between breeding pairs and even individuals if '
              'space is limited. The presence of other parakeets '
              'encourages a pair to breed, which is why breeding in groups is better.',
          //Now to ensure our app doesn't do anything funky with this
          //paragraph, we want to let the system know that if there is
          //too many words for the available space, then use elipses
          //after the last word that could fit in the container. Also,
          //justify the text so it will come out in a block filling
          //up the container. We also state that we want a max of 14
          //lines, so if the text takes 15 lines the last line will be
          //taken out and replaced with elipses
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.justify,
          maxLines: 14,
          //We want the font to be of size 14 and color black45
          //(lighter shade of black)
          style: TextStyle(fontSize: 14, color: Colors.black45),
          //To ensure the text looks the same for different screen
          //sizes, we use a soft wrap.
          softWrap: true,
        ),
      ),
    ],
  ),
);
//Now, we have finished declaring the _buildMainContainer(), which builds the
//section of info which pertains to all birds together with general info on them
//as a species.



//Here we declare predefined strings which we can pass into function calls for
//building the bird's individual info containers
//(_buildBirdContainer(..., String desc,...)).
//This way we can avoid writing this long string in the function call and we can
//use the naming of these strings to know which bird's container we are creating
var Pharaohdesc = 'He is the smartest of the group, when the cage opens for them to go out you can always find Pharaoh leaving and entering on his own. No assistance needed. ';
var Blueberrydesc = 'She is the oldest of them. She loves to dig her nest and when she is loose around the house she loves to fly (even more than the rest).';
var Spiderdesc = 'He is the fluffiest of the group. He loves to sleep and eat his favorite food (Millets). When they are out of the cage he prefers to lounge around. Contrary to his name. ';
var Pepitadesc = 'She is the youngest and most agile of the bunch. It is always a task trying to catch Pepita when its time to go back in the cage. She is the first up and showered';
var Pename = "Pepita";
var Phname = "Pharaoh";
var Bname = "Blueberry";
var SMname = "Spider-Man";



//Now lets build the column container which will hold the 4 individual Bird
//info containers [ _buildInfoContainer() ],  and be placed directly under the
//text "Lets Meet Them..." which is under the build for the MainContainer
Widget _buildInfoContainer() => Container(
  //We want this column containers's width to be the same as the width of each
  //bird's info container, so we do not predefine it for this container.
  //However, we do want our container to have a height greater than the height
  //of all 4 bird info containers stacked on top of each other. So we use 900
  //px in this case
  height: 850,
  child: Column(
    //We want these bird info containers to be spaced evenly within this
    //container
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children:[
      //and we then build each individual bird's info container
      //Passing in these parameter. NOte using this method, in the
      //birdPageNav, we must get these attributes by using this.feeds for example
      birdPageNav(
          name: Phname,
          feeds: 10,
          views: 22,
          likes: 15,
          desc: Pharaohdesc,
          pic: 'assets/Pharaoh2.jpg',
      ),

      birdPageNav(
        name: Bname,
        feeds: 5,
        views: 10,
        likes: 7,
        desc: Blueberrydesc,
        pic: 'assets/Blueberry2.jpg',
      ),

      birdPageNav(
        name: SMname,
        feeds: 12,
        views: 18,
        likes: 17,
        desc: Spiderdesc,
        pic: 'assets/Spider-Man2.jpg',
        ),

      birdPageNav(
        name: Pename,
        feeds: 8,
        views: 15,
        likes: 15,
        desc: Pepitadesc,
        pic: 'assets/Pepita.jpeg',
        ),
    ],
  ),
);
//Now we have declared the _buildInfoContainer() which takes no parameters and
//builds/formats the 4 individual bird info containers.



//We then will declare the _buildBottomContainer() which acts as an artist
//signature for the app and lies at the very bottom of
//Main Container(has background image). It has the school name, semester,
// course, and my name
Widget _buildBottomContainer() => Container(
  //We will want this to take up the bottom 60 px of vertical space on the
  //Main Container and we want it to expand almost the entire screen length
  height: 60,
  width: 300,
  //To get the signature in lines underneath one another we make this a
  //column container
  child: Column(
    //We align the signature to the left bottom side of the screen
    crossAxisAlignment: CrossAxisAlignment.start,  //Row start = left side
    children: [
      //We then declare each text segment of the artist signature
      //School name and semester:
      Text('CSUSM: Fall 2020',
        //Now we want each text segment to be white, bold, and
        //50% transparent
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.white.withOpacity(0.5),
        ),
      ),
      //Now we want to declare the
      //Course:
      Text('CS 481: Mobile Programming',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.white.withOpacity(0.5),
        ),
      ),
      //Now we want to declare my
      //Name:
      Text('Jesus Mercado',
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.white.withOpacity(0.5),
        ),
      ),
    ],
  ),
);
//Now we have finished declaring the artist signature for yhe app which will be
//placed at the very bottom of the app.



//Here we will define the function to easily create the icons seen in each bird's
//individual page. Note that this icon, unlike the icon in birdPageNav, will not
//be stateful and the only way to turn on/off the like button is to do so from the
//birdPageNav container on the home screen (click the heart icon)
//integer views holds the number of view we currently have and isviewed lets
//the icon we build know which state to take
class viewContainer extends StatefulWidget {
  bool isviewed;
  int views;
  //we get the parameters we passed in from this, as described above
  viewContainer({Key key, this.views, this.isviewed}) : super(key: key);
  @override
  _viewContainerState createState() => _viewContainerState();
}

class _viewContainerState extends State<viewContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      //We want our icon and data to be within a column container of size
      //60 x 30 px. So that it fits almost the entire vertical area of the
      //container it will be built inside of.
      height: 82,
      width: 50,
      padding: const EdgeInsets.all(0),
      child: Column(
        //We want this icon container to begin from the top of the parent
        //column container and we want the objects to align to the center of a row
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            height: 30,
            width: 80,
            //We declare the Icon at the top of this small column container
            //which will use isviewed to determine which logo to display
            child:IconButton(
              icon: (widget.isviewed ? Icon(Icons.visibility, color: Colors.blue,) : Icon(Icons.visibility_outlined, color: Colors.white,)  ),
            ),
          ),

         Container(
           child: Column(
             children: [
               //$ so when the value changes, our printed value changes as well
               Text('${widget.views}', style: TextStyle(
                 fontSize: 14,
               ),
               ),
               //We declare a explicit string which states what the icon means and
               //gives a unit to the integer string, in this case the unit is views.
               //This is directly under the printed view parameter value under
               //the icon, and it is also centered with size 10 font.
               Text('views',
                 style: TextStyle(
                   fontSize: 14,
                 ),
               ),
             ],
           )
         )
          //We declare a string which represents an integer to display the
          //number views this specific bird has gotten. Under the
          //Icon and centered.
        ],
      ),
    );
  }
}
//We finish declaring the viewContainer function
//and we can proceed to the next icon for the stateless individual pages.



//Now we have the declaration for the likes icon, tis function will be called
//when making the bird's indiviual info containers for their page
//likes and isliked similar to views and isviewed
class likesContainer extends StatefulWidget {
  int likes;
  bool isliked = true;
  likesContainer({Key key, this.likes, this.isliked}) : super(key: key);
  @override
  _likesContainerState createState() => _likesContainerState();
}

class _likesContainerState extends State<likesContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 82,
      width: 50,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 30,
              width: 80,
              child: IconButton(
                icon: (widget.isliked ? Icon(
                  Icons.favorite_outline, color: Colors.white,) : Icon(
                  Icons.favorite, color: Colors.red,)),
                onPressed: null,
              ),
          ),
          //We declare the Icon at the top of this small column container

          Container(
            child: Column(
              children: [
                Text('${widget.likes}', style: TextStyle(
                  fontSize: 14,
                ),
                ),
                //We declare a explicit string which states what the icon means and
                //gives a unit to the integer string, in this case the unit is views.
                //This is directly under the printed view parameter value under
                //the icon, and it is also centered with size 10 font.
                Text('likes',
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
//We finish defining the likes container and we can now easily call it
//in the fucntion which will create the birds individual info containers ofr their pade
//(_buildbirdcontainer)


//Last we define the fucnition for the feeds icon in the bord's individual
//pages
class feedsContainer extends StatefulWidget {
  int feeds;
  bool isfed = true;
  feedsContainer({Key key, this.feeds, this.isfed}) : super(key: key);
  @override
  _feedsContainerState createState() => _feedsContainerState();
}

class _feedsContainerState extends State<feedsContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 82,
      width: 50,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
              height: 30,
              width: 80,
              child: IconButton(
                icon: (widget.isfed ? Icon(Icons.fastfood_outlined, color: Colors.white,) : Icon(Icons.fastfood, color: Colors.teal,)),
                  onPressed: null,
              ),
          ),
          //We declare the Icon at the top of this small column container

          Container(
            child: Column(
              children: [
                Text('${widget.feeds}', style: TextStyle(
                  fontSize: 14,
                ),
                ),
                //We declare a explicit string which states what the icon means and
                //gives a unit to the integer string, in this case the unit is views.
                //This is directly under the printed view parameter value under
                //the icon, and it is also centered with size 10 font.
                Text('feeds',
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
//We declared the icon for feeds which will display the current feeds for the
//bird whose page we are on



//Here we will define the stateful widget which will replace the birds
// container onm home page
  class birdPageNav extends StatefulWidget {
  //To pass parameters/data to a stateful widget define what yoou will pass here
  final String name;
  int feeds;
  int views;
  int likes;
  String desc;
  String pic;

  //make the prototype with the the this method mentioned above
  birdPageNav({Key key, this.name, this.feeds, this.views, this.likes, this.desc, this.pic,}) : super(key: key);
  @override
  _birdPageNavState createState() => _birdPageNavState();
  }

  class _birdPageNavState extends State<birdPageNav>
  {
    //boolean values to pass as data allowing our icons to know what state to be in
  bool _isClicked = true;      //for views icon
  bool considereed = false;
  bool isliked = true;      //for likes
  bool isfed = true;       //for feeds

  @override
  Widget build(BuildContext context) {
    //this gesture will allow us to know when the container is clicked so that
    //we can redirect pages when it is clicked and pass data accordingly
  return GestureDetector(
    onTap: _updateviews,
       child: Container(
            height: 200,
            width: 350,
             //use stack to put text and icons over an image
             child: Stack(
                fit: StackFit.expand,
                children: [
                  Image.asset(widget.pic, fit: BoxFit.cover,),
                  Container(
                      height: 200,
                      width: 175,
                     child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                            Text(widget.name,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                      ),
                  ),
                      Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                                Container(
                                    padding: const EdgeInsets.all(0),
                            //   color: Colors.red,
                                    height: 80,
                                    width: 175,
                            //    color: Colors.white,
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                         viewContainer(
                                           views: widget.views,
                                            isviewed: this.considereed,
                                          ),

                                          //likes icon widget built here for home screen
                                          //interactive widget.
                                        //below we use the actual code for each button so that when passing boolean data,
                                        //we can easily access it since it is within this fucntion (isliked instead of widgte.isliked)
                                        //and the state is saved within this function so its value applies to all of these icons (data is consistent)
                                          Container(
                                              height: 82,
                                              width: 50,
                                              child: Column(
                                                  mainAxisAlignment: MainAxisAlignment.center,
                                                  children: [
                                                     Container(
                                                       height: 30,
                                                        width: 80,
                                                        child: IconButton(
                                                            icon: (isliked ? Icon(Icons.favorite_outline, color: Colors.white,) : Icon(Icons.favorite, color: Colors.red,)),
                                                            onPressed: _updatelikes,
                                                    )
                                                ),

                                                    Container(
                                                      child: Column(
                                                        children: [
                                                          Text('${widget.likes}', style: TextStyle(
                                                            fontSize: 14,
                                                          ),
                                                          ),
                                                          //We declare a explicit string which states what the icon means and
                                                          //gives a unit to the integer string, in this case the unit is views.
                                                          //This is directly under the printed view parameter value under
                                                          //the icon, and it is also centered with size 10 font.
                                                          Text('likes',
                                                            style: TextStyle(
                                                              fontSize: 14,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                         Container(
                                              height: 82,
                                              width: 50,
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Container(
                                                      height: 30,
                                                      width: 80,
                                                      child: IconButton(
                                                        icon: (isfed ? Icon(Icons.fastfood_outlined, color: Colors.white,) : Icon(Icons.fastfood, color: Colors.teal,)),
                                                        onPressed: _updatefeeds,
                                                      )
                                                  ),

                                                  //We declare the Icon at the top of this small column container
                                                  Container(
                                                    child: Column(
                                                      children: [
                                                        Text('${widget.feeds}', style: TextStyle(
                                                          fontSize: 14,
                                                        ),
                                                        ),
                                                        //We declare a explicit string which states what the icon means and
                                                        //gives a unit to the integer string, in this case the unit is views.
                                                        //This is directly under the printed view parameter value under
                                                        //the icon, and it is also centered with size 10 font.
                                                        Text('feeds',
                                                          style: TextStyle(
                                                            fontSize: 14,
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                              ),
                                            ],
                                          ),
                                        ),
                                   ],
                              ),
                         ),

                                Container(
                                    padding: const EdgeInsets.only(left: 3, right: 5),
                                    color: Colors.black.withOpacity(0.5),
                                    width: 175,
                                    child: Text(widget.desc,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                        textAlign: TextAlign.justify,
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold,
                                            ),
                                        ),
                                ),
                           ],
                        )
                      ],
                     ),
                  ),
                ],
             ),
        ),
    );
  }

  //Now we will inform the system when to do when the like button is pressed
  //we use isliked as a boolean switch and we increment likes when it is cliked
  //and decrement when it is not clicked. Note by changing isliked we then have a new
  //icon appear
  void _updatelikes() {
    setState(() {
      if (isliked) {
        widget.likes += 1;
        isliked = false;
        }
      else {
        widget.likes -= 1;
        isliked = true;
        }
      }
      );
    }
//ame logic but for the feeds icon button
    void _updatefeeds() {
    setState(() {
      if (isfed) {
        widget.feeds += 1;
        isfed = false;
        }
      else {
        widget.feeds -= 1;
        isfed = true;
        }
      }
    );
  }
//when we view a bird's page for the first time
  void _updateviews() {
    setState(() {
      //here we need to know which page to redirect to , so we look for the nam
      //and call the appropriate screen for that bird.
      if (widget.name == "Pepita") {
        //if the container is cliked and we havent visited Pepita's page yet,
        //Increment the view amd change considered to true. We should noe leave
        //considered alone forever. We only want it to update one time when the very first
        //time the user opens this birds page occurs.
        if (_isClicked && !considereed) {
          widget.views += 1;
          considereed = true;
        }
        //send us to pepitas page passing data using the this. method
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              PepitaPage(
                firstView: considereed,
                views: widget.views,
                likes: widget.likes,
                feeds: widget.feeds,
                liked: isliked,
                fed: isfed,
              ),
          ),
        );
      }
      //same logic for the other birds
      else if (widget.name == "Pharaoh") {
        if (_isClicked && !considereed) {
          widget.views += 1;
          considereed = true;
        }

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              PharaohPage(
                firstView: considereed,
                views: widget.views,
                likes: widget.likes,
                feeds: widget.feeds,
                liked: isliked,
                fed: isfed,
              ),
          ),
        );
      }
      else if (widget.name == "Spider-Man") {
        if (_isClicked && !considereed) {
          widget.views += 1;
          considereed = true;
        }

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              SpiderPage(
                firstView: considereed,
                views: widget.views,
                likes: widget.likes,
                feeds: widget.feeds,
                liked: isliked,
                fed: isfed,
              ),
          ),
        );
      }
      else if (widget.name == "Blueberry") {
        if (_isClicked && !considereed) {
          widget.views += 1;
          considereed = true;
        }

        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>
              BlueberryPage(
                firstView: considereed,
                views: widget.views,
                likes: widget.likes,
                feeds: widget.feeds,
                liked: isliked,
                fed: isfed,
                  ),
              ),
            );
          }
        }
      );
    }
  }

//Navigation to each birds page
class PharaohPage extends StatelessWidget {
  int likes;
  int feeds;
  int views;
  bool firstView;
  bool liked;
  bool fed;

  PharaohPage({this.likes, this.feeds, this.views, this.firstView, this.liked, this.fed}) {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pharaoh'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/Jungle.jpg',
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _buildBirdContainer(Phname, 'assets/Pharaoh.jpg', 'Male', 'Me', Pharaohdesc, this.views, this.feeds, this.likes, context, this.firstView, this.liked, this.fed),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

//Navigation to each birds page
class BlueberryPage extends StatelessWidget {
  int likes;
  int feeds;
  int views;
  bool firstView;
  bool liked;
  bool fed;

  BlueberryPage({this.likes, this.feeds, this.views, this.firstView, this.liked, this.fed}) {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Blueberry'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/Jungle.jpg',
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _buildBirdContainer(Bname, 'assets/Blueberry.jpg', 'Female', 'Sister', Blueberrydesc, this.views, this.feeds, this.likes, context, this.firstView, this.liked, this.fed),
              ],
            ),
          ),
        ],
      ),
    );
  }
}


//Navigation to each birds page
class SpiderPage extends StatelessWidget {
  int likes;
  int feeds;
  int views;
  bool firstView;
  bool liked;
  bool fed;

  SpiderPage({this.likes, this.feeds, this.views, this.firstView, this.liked, this.fed}) {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Spider-Man'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/Jungle.jpg',
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _buildBirdContainer(SMname, 'assets/Spider-Man.jpg', 'Male', 'Brother', Spiderdesc, this.views, this.feeds, this.likes, context, this.firstView, this.liked, this.fed),
              ],
            ),
          ),
        ],
      ),
    );
  }
}



//Navigation to each birds page
class PepitaPage extends StatelessWidget {
  int likes;
  int feeds;
  int views;
  bool firstView;
  bool liked;
  bool fed;

  PepitaPage(
      {this.likes, this.feeds, this.views, this.firstView, this.liked, this.fed}) {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:  Text('Pepita'),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: [
          Image.asset('assets/Jungle.jpg',
            fit: BoxFit.cover,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 3,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                _buildBirdContainer(Pename, 'assets/Pepita.jpeg', 'Female', 'Mom', Pepitadesc, this.views, this.feeds, this.likes, context, this.firstView, this.liked, this.fed),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
//Code is complete